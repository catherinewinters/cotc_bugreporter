<?php

namespace App\Controller;

use App\Entity\BugReport;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class BugReportAdminController extends AbstractController
{
    /**
     * @Route("/admin/bugreport/new")
     */
    public function new(EntityManagerInterface $em)
    {
        $current_time = new \DateTime();
        $bugreport = new BugReport();
        $bugreport->setTitle('Bug Report Test')
            ->setDescription('Bug report description')
            ->setTimestamp($current_time)
            ->setSlug('bug-report-'.rand(100, 999))
            ->setUuid('test')
            ->setImageName('test');
            // ->setImageSize(100);

        $em->persist($bugreport);
        $em->flush();

        return new Response(sprintf(
            'New bug report id: #%d',
            $bugreport->getId()
            // $bugreport->getSlug()
        ));
    }
}
