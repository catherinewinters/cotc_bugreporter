<?php
// src/Controller/DefaultController.php
namespace App\Controller;

use App\Entity\BugReport;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

use Symfony\Component\HttpFoundation\Response;
use Cocur\Slugify\Slugify;

class BugReportController extends Controller
{
    // Debug form at /bugreport/new
    public function new(Request $request)
    {
        $bugreport = new BugReport();

        $form = $this->createFormBuilder($bugreport)
            ->add('Title', TextType::class)
            ->add('Timestamp', DateType::class, array('required' => false, 'data' => new \DateTimeImmutable()))
            ->add('Slug', TextType::class)
            ->add('uuid', TextType::class)
            ->add('save', SubmitType::class, array('label' => 'Submit bug report'))
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $bugreport = $form->getData();

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($bugreport);
            $entityManager->flush();

            return $this->redirectToRoute('bug_report_success');
        }

        return $this->render('bugreport/new.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    // Accept POST requests at /bugreport/post
    public function post(Request $request)
    {
        $data = json_decode($request->getContent(), true);

        $bugreport = new BugReport();
        $bugreport->setTitle($data['Title']);
        $bugreport->setUuid($data['uuid']);

        $slugify = new Slugify();
        $slugify = $slugify->slugify($data['Title']);
        $bugreport->setSlug($slugify);

        $bugreport->setTimestamp(new \DateTimeImmutable());

        $em = $this->getDoctrine()->getManager();
        $em->persist($bugreport);
        $em->flush();

        return new Response('Bug report saved.');
    }
}
