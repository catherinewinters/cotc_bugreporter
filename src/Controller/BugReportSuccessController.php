<?php
// src/Controller/BugReportSuccessController.php
namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;

class BugReportSuccessController
{
    public function success()
    {
        return new Response(
            '<html><body>Success!</body></html>'
        );
    }
}
