COTC BugReporter 0.1.1

This is a simple Symfony 4.1-based server and Unity 2017.4/2018.1 client for
submitting bug reports from within Unity games.

The relevant Symfony/PHP components are in /src/Controller
The relevant C#/Mono components are in /unity

Dependencies:
It uses Doctrine and VichUploader to handle fields and screenshot uploading,
and EasyAdmin for debugging.
