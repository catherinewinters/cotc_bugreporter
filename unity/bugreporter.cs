using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEditor;

public class BugReporter : MonoBehaviour {

	public string BugReporterServerAddress;

	void Start () {
		Debug.Log (SystemInfo.deviceUniqueIdentifier);
		if (BugReporterServerAddress == null) {
			EditorGUILayout.HelpBox ("Critical error: You must enter a server address in order to save bug reports.", MessageType.Error);
		} else if (BugReporterServerAddress.Substring (0, 6) == "http://") {
			EditorGUILayout.HelpBox ("Warning: http:// is insecure. Use https:// instead.", MessageType.Warning);
		} else if (BugReporterServerAddress.Contains ("http://") == false || BugReporterServerAddress.Contains ("https://") == false) {
			EditorGUILayout.HelpBox ("Critical error: This is not a valid server address.", MessageType.Warning);
		}
	}

	public void SubmitBugReport() {
		StartCoroutine (Upload ());
	}

	IEnumerator Upload() {
		System.Guid myGUID = System.Guid.NewGuid ();

		string formdata = @"{
			'Title': 'Test title',
			'uuid': '" + myGUID.ToString () + @"'
		}";

		formdata = formdata.Replace ("'", "\"");

		// We have to initialize the request using PUT instead of POST for terrible Unity-related nonsense reasons.
		UnityWebRequest request = UnityWebRequest.Put (BugReporterServerAddress, formdata);
		request.method = "POST"; // It works fine if we replace UnityWebRequest.method with POST after the fact.

		request.SetRequestHeader ("Content-Type", "application/json");

		yield return request.SendWebRequest ();

		if (request.isNetworkError || request.isHttpError) {
			Debug.Log (request.error);
		} else {
			Debug.Log ("Form submission complete!");
		}
	}
}
